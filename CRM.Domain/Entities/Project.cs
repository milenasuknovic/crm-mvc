﻿using CRM.Domain.Enums;

namespace CRM.Domain.Entities
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}
