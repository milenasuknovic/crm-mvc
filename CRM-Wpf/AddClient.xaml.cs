﻿using CRM.Data.EF;
using CRM.Data.Interfaces;
using CRM_Wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CRM_Wpf
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class AddClient : Window
    {
        public AddClient(MainViewModel main)
        {
            InitializeComponent();
            DataContext = new AddClientViewModel(main);
        }

        private void close(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

    }
}