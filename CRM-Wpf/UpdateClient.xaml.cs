﻿using CRM.Data.EF;
using CRM.Data.Interfaces;
using CRM.Domain.Entities;
using CRM_Wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CRM_Wpf
{
    /// <summary>
    /// Interaction logic for UpdateClient.xaml
    /// </summary>
    public partial class UpdateClient : Window
    {
        public UpdateClient(MainViewModel main, Client client)
        {
            InitializeComponent();
            DataContext = new UpdateClientViewModel(main, client);
        }

        private void btnCancelClient_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
