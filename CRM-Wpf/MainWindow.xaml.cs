﻿using CRM_Wpf.ViewModels;
using System.Windows;

namespace CRM_Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();

            _viewModel = new MainViewModel();
            DataContext = _viewModel;
        }

        private void btnAddClients_Click(object sender, RoutedEventArgs e)
        {
            var addClient = new AddClient(_viewModel);
            addClient.Show();
        }

        private void btnAddContacts_Click(object sender, RoutedEventArgs e)
        {
            var addContact = new AddContact(_viewModel, _viewModel.SelectedClient.Id);
            addContact.Show();
        }

        private void btnAddProjects_Click(object sender, RoutedEventArgs e)
        {
            var addProject = new AddProject(_viewModel, _viewModel.SelectedClient.Id);
            addProject.Show();
        }

        private void btnUpdateClients_Click(object sender, RoutedEventArgs e)
        {
            var updateClient = new UpdateClient(_viewModel, _viewModel.SelectedClient);
            updateClient.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var updateContact = new UpdateContact(_viewModel, _viewModel.SelectedContact);
            updateContact.Show();
        }

        private void btnUpdateProject(object sender, RoutedEventArgs e)
        {


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var updateProject = new UpdateProject(_viewModel, _viewModel.SelectedProject);
            updateProject.Show();
        }
    }
}
