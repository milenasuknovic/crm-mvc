﻿using CRM.Data.Interfaces;
using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private Client _client = new Client();
        private Project _project = new Project();
        private Contact _contact = new Contact();
        private Project _selectedProject;
        private Client _selectedClient;
        private Contact _selectedContact;
        private ClientService _clientService;
        private ContactService _contactService;
        private ProjectService _projectService;
        public AddContactViewModel ContactAddView { get; set; }
        public ObservableCollection<Client> Clients { get; set; }
        public ObservableCollection<Contact> Contacts { get; set; }
        public ObservableCollection<Project> Projects { get; set; }

        public Contact SelectedContact
        {
            get { return _selectedContact; }
            set
            {
                _selectedContact = value;
            }
        }

        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
            }
        }

        public Client SelectedClient
        {
            get { return _selectedClient; }
            set
            {
                _selectedClient = value;
                SelectClient();
            }
        }

        //public RelayCommand OnClientSelectionCommand
        //{
        //    get;
        //    private set;
        //}

        public MainViewModel() : base()
        {

            _clientService = new ClientService();
            _contactService = new ContactService();
            _projectService = new ProjectService();

            LoadClients();

            //OnClientSelectionCommand = new RelayCommand(SelectedClient);
            Projects = new ObservableCollection<Project>();
            Contacts = new ObservableCollection<Contact>();
        }

        public ICommand DeleteClient
        {
            get { return new DelegateCommand<object>(DeleteForClient); }
        }

        public ICommand DeleteProject
        {
            get { return new DelegateCommand<object>(DeleteForProject); }
        }

        public ICommand DeleteContact
        {
            get { return new DelegateCommand<object>(DeleteForContact); }
        }

        public ICommand SearchClient
        {
            get { return new DelegateCommand<object>(SearchForClient); }
        }

        public ICommand SearchContact
        {
            get { return new DelegateCommand<object>(SearchForContact); }
        }

        public ICommand SearchProject
        {
            get { return new DelegateCommand<object>(SearchForProject); }
        }

        private void SearchForProject(object obj)
        {
            List<Project> searchList = Projects.Where(x => x.Name.StartsWith(TxtSearchProject)).ToList();
            this.Projects = new ObservableCollection<Project>(searchList);
            NotifyPropertyChanged("Projects");
        }

        private void SearchForContact(object obj)
        {
            List<Contact> searchList = Contacts.Where(x => x.FirstName.StartsWith(TxtSearchContact)).ToList();
            this.Contacts = new ObservableCollection<Contact>(searchList);
            NotifyPropertyChanged("Contacts");
        }

        public ICommand GetAllClients
        {
            get { return new DelegateCommand<object>(RefreshForClients); }
        }

        public ICommand GetAllContacts
        {
            get { return new DelegateCommand<object>(RefreshForContacts); }
        }

        public ICommand GetAllProjects
        {
            get { return new DelegateCommand<object>(RefreshForProjects); }
        }

        private void RefreshForProjects(object obj)
        {
            throw new NotImplementedException();
        }

        private void RefreshForContacts(object obj)
        {
            NotifyPropertyChanged("Contacts");
        }

        private void RefreshForClients(object obj)
        {
            LoadClients();
            NotifyPropertyChanged("Clients");
        }

        private void SearchForClient(object obj)
        {
            List<Client> searchList = Clients.Where(x => x.Name.StartsWith(TxtSearchClient)).ToList();
            this.Clients = new ObservableCollection<Client>(searchList);
            NotifyPropertyChanged("Clients");

        }

        private void DeleteForClient(object context)
        {
            if (SelectedClient == null)
                return;
            Client clinetForDelete = Clients.Where(x => x.Id == SelectedClient.Id).FirstOrDefault();
            _clientService.DeleteClient(clinetForDelete);
            Clients.Remove(clinetForDelete);
            MessageBox.Show("Client deleted!");
        }

        private void DeleteForContact(object context)
        {
            if (SelectedContact == null)
                return;
            _contactService.DeleteContact(SelectedContact);
            Contacts.Remove(Contacts.Where(x => x.Id == SelectedContact.Id).FirstOrDefault());

            MessageBox.Show("Contact deleted!");
        }

        private void DeleteForProject(object context)
        {
            if (SelectedProject == null)
                return;
            _projectService.DeleteProject(SelectedProject);
            Projects.Remove(Projects.Where(x => x.Id == SelectedProject.Id).FirstOrDefault());

            MessageBox.Show("Project deleted!");
        }

        private void SelectClient()
        {
            if (SelectedClient == null)
                return;

            List<Contact> contacts = _contactService.GetAllContact(SelectedClient.Id);
            ObservableCollection<Contact> contactOb = new ObservableCollection<Contact>(contacts);

            foreach (var contact in contactOb)
            {
                Contacts.Add(contact);
            }

            List<Project> projects = _projectService.GetAllProject(SelectedClient.Id);
            ObservableCollection<Project> projectOb = new ObservableCollection<Project>(projects);

            foreach (var project in projectOb)
            {
                Projects.Add(project);
            }
        }

        public void LoadClients()
        {
            List<Client> clients = _clientService.GetAllClients();
            ObservableCollection<Client> clientsOb = new ObservableCollection<Client>(clients);

            Clients = clientsOb;
        }

        public string TxtSearchClient
        {
            get { return _client.Name; }
            set { _client.Name = value; }
        }

        public string TxtSearchContact
        {
            get { return _contact.FirstName; }
            set { _contact.FirstName = value; }
        }

        public string TxtSearchProject
        {
            get { return _project.Name; }
            set { _project.Name = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
