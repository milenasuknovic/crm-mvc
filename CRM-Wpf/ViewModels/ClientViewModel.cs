﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Wpf.ViewModels
{
    public class ClientViewModel
    {
        private Client client = new Client();
        ClientService clientService;

        private Contact contact = new Contact();
        ContactService contactService;
        public ObservableCollection<Contact> Contacts { get; set; }

        public ClientViewModel()
        {
            clientService = new ClientService();
            contactService = new ContactService();
            LoadClients();
        }
        public ObservableCollection<Client> Clients { get; set; }

        public void LoadClients()
        {
            List<Client> clients = clientService.GetAllClients();
            ObservableCollection<Client> clientsOb = new ObservableCollection<Client>(clients);
            Clients = clientsOb;

            List<Contact> contacts = contactService.GetAllContact(contact.ClientId);
            ObservableCollection<Contact> contactOb = new ObservableCollection<Contact>(contacts);

            Contacts = contactOb;
        }

        public string TxtName
        {
            get { return client.Name; }
            set { client.Name = value; }
        }

        public string TxtAddress
        {
            get { return client.Address; }
            set { client.Address = value; }
        }

        public string TxtCity
        {
            get { return client.City; }
            set { client.City = value; }
        }

        public string TxtZipCode
        {
            get { return client.ZipCode; }
            set { client.ZipCode = value; }
        }

        public string TxtState
        {
            get { return client.State; }
            set { client.State = value; }
        }

        public string TxtPhoneNumber
        {
            get { return client.PhoneNumber; }
            set { client.PhoneNumber = value; }
        }

        public string TxtWebsite
        {
            get { return client.Website; }
            set { client.Website = value; }
        }

        public string TxtNotes
        {
            get { return client.Notes; }
            set { client.Notes = value; }
        }
    }
}
