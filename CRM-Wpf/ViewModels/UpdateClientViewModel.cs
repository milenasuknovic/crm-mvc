﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{

    public class UpdateClientViewModel : INotifyPropertyChanged
    {
        private Client _client = new Client();
        private ClientService _clientService;
        public MainViewModel main;

        public event PropertyChangedEventHandler PropertyChanged;

        public UpdateClientViewModel(MainViewModel main, Client client) : base()
        {
            _clientService = new ClientService();
            this._client = client;
            this.main = main;
        }

        public ICommand UpdateClient
        {
            get { return new DelegateCommand<object>(FuncToCall); }
        }

        private void FuncToCall(object context)
        {
            TxtAddress = _client.Address;
            TxtCity = _client.City;
            TxtName = _client.Name;
            TxtNotes = _client.Notes;
            TxtPhoneNumber = _client.PhoneNumber;
            TxtState = _client.State;
            TxtWebsite = _client.Website;
            TxtZipCode = _client.ZipCode;

            main.Clients.Remove(_client);
            _clientService.UpdateClient(_client);
            main.Clients.Add(_client);

            MessageBox.Show("Client updated!");
        }

        public string TxtName
        {
            get { return _client.Name; }
            set { _client.Name = value; }
        }

        public string TxtAddress
        {
            get { return _client.Address; }
            set { _client.Address = value; }
        }

        public string TxtCity
        {
            get { return _client.City; }
            set { _client.City = value; }
        }

        public string TxtZipCode
        {
            get { return _client.ZipCode; }
            set { _client.ZipCode = value; }
        }

        public string TxtState
        {
            get { return _client.State; }
            set { _client.State = value; }
        }

        public string TxtPhoneNumber
        {
            get { return _client.PhoneNumber; }
            set { _client.PhoneNumber = value; }
        }

        public string TxtWebsite
        {
            get { return _client.Website; }
            set { _client.Website = value; }
        }

        public string TxtNotes
        {
            get { return _client.Notes; }
            set { _client.Notes = value; }
        }
    }
}
