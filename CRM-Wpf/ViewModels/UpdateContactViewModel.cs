﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System.Windows;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    public class UpdateContactViewModel
    {
        private Contact _contact = new Contact();
        private ContactService _contactService;
        public MainViewModel main;
        public UpdateContactViewModel(MainViewModel mainView, Contact contact)
        {
            _contactService = new ContactService();
            _contact = contact;
            this.main = mainView;
        }

        public ICommand UpdateContact
        {
            get { return new DelegateCommand<object>(FuncToCall); }
        }

        private void FuncToCall(object context)
        {

            TxtFirstName = _contact.FirstName;
            TxtLastName = _contact.LastName;
            TxtRole = _contact.Role;
            TxtEmail = _contact.Email;
            TxtPhone = _contact.Phone;
            TxtAddress = _contact.Address;
            TxtCity = _contact.City;
            TxtZipCode = _contact.ZipCode;
            TxtState = _contact.State;

            main.Contacts.Remove(_contact);
            _contactService.UpdateContact(_contact);
            main.Contacts.Add(_contact);

            MessageBox.Show("Contact updated!");
        }

        public string TxtFirstName
        {
            get { return _contact.FirstName; }
            set { _contact.FirstName = value; }
        }

        public string TxtLastName
        {
            get { return _contact.LastName; }
            set { _contact.LastName = value; }
        }

        public string TxtRole
        {
            get { return _contact.Role; }
            set { _contact.Role = value; }
        }
        public string TxtEmail
        {
            get { return _contact.Email; }
            set { _contact.Email = value; }
        }
        public string TxtPhone
        {
            get { return _contact.Phone; }
            set { _contact.Phone = value; }
        }

        public string TxtAddress
        {
            get { return _contact.Address; }
            set { _contact.Address = value; }
        }
        public string TxtCity
        {
            get { return _contact.City; }
            set { _contact.City = value; }
        }
        public string TxtZipCode
        {
            get { return _contact.ZipCode; }
            set { _contact.ZipCode = value; }
        }

        public string TxtState
        {
            get { return _contact.State; }
            set { _contact.State = value; }
        }
    }
}
