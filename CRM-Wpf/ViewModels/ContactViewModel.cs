﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Wpf.ViewModels
{
    public class ContactViewModel
    {
        private Contact contact = new Contact();
        ContactService contactService;

        public ContactViewModel()
        {
            contactService = new ContactService();
            LoadContacts();
        }
        public ObservableCollection<Contact> Contacts { get; set; }

        public void LoadContacts()
        {
            List<Contact> contacts = contactService.GetAllContact(contact.ClientId);
            ObservableCollection<Contact> contactOb = new ObservableCollection<Contact>(contacts);

            Contacts = contactOb;
        }
    }
}
