﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System.Windows;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    public class AddProjectViewModel
    {
        private Project project = new Project();
        private ProjectService projectService;
        public int ClientId { get; set; }
        public MainViewModel mainView;

        public AddProjectViewModel(MainViewModel mainView, int clientId)
        {
            projectService = new ProjectService();
            ClientId = clientId;
            this.mainView = mainView;
        }

        public ICommand SaveProject
        {
            get { return new DelegateCommand<object>(FuncToCall); }
        }

        private void FuncToCall(object context)
        {
            project.ClientId = ClientId;
            project.Name = TxtName;
            project.Category = CRM.Domain.Enums.Category.Internal;
            project.Description = TxtDescription;

            projectService.InsertProject(project);
            mainView.Projects.Add(project);
            MessageBox.Show("Project added!");
        }
        public string TxtName
        {
            get { return project.Name; }
            set { project.Name = value; }
        }

        public string TxtDescription
        {
            get { return project.Description; }
            set { project.Description = value; }
        }
        //public string TxtCategory
        //{
        //    get { return project.Category; }
        //    set { project.Category = CRM.Domain.Enums.Category.Internal; }
        //}
    }
}
