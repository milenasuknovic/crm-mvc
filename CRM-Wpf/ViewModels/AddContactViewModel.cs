﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    public class AddContactViewModel
    {
        private Contact _contact = new Contact();
        private ContactService _contactService;
        public int ClientId { get; set; }
        public MainViewModel mainView;

        public AddContactViewModel(MainViewModel mainView, int clientId)
        {
            _contactService = new ContactService();
            ClientId = clientId;
            this.mainView = mainView;
        }

        public ICommand SaveContact
        {
            get { return new DelegateCommand<object>(FuncToCall); }
        }

        private void FuncToCall(object context)
        {
            _contact.ClientId = ClientId;
            _contact.FirstName = TxtFirstName;
            _contact.LastName = TxtLastName;
            _contact.Role = TxtRole;
            _contact.Email = TxtEmail;
            _contact.Phone = TxtPhone;
            _contact.Address = TxtAddress;
            _contact.City = TxtCity;
            _contact.ZipCode = TxtZipCode;
            _contact.State = TxtState;

            _contactService.InsertContact(_contact);
            mainView.Contacts.Add(_contact);

            MessageBox.Show("Contact added!");
        }

        public string TxtFirstName
        {
            get { return _contact.FirstName; }
            set { _contact.FirstName = value; }
        }

        public string TxtLastName
        {
            get { return _contact.LastName; }
            set { _contact.LastName = value; }
        }

        public string TxtRole
        {
            get { return _contact.Role; }
            set { _contact.Role = value; }
        }
        public string TxtEmail
        {
            get { return _contact.Email; }
            set { _contact.Email = value; }
        }
        public string TxtPhone
        {
            get { return _contact.Phone; }
            set { _contact.Phone = value; }
        }

        public string TxtAddress
        {
            get { return _contact.Address; }
            set { _contact.Address = value; }
        }
        public string TxtCity
        {
            get { return _contact.City; }
            set { _contact.City = value; }
        }
        public string TxtZipCode
        {
            get { return _contact.ZipCode; }
            set { _contact.ZipCode = value; }
        }

        public string TxtState
        {
            get { return _contact.State; }
            set { _contact.State = value; }
        }
    }
}
