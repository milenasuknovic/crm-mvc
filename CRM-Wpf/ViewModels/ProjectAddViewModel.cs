﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    public class ProjectAddViewModel
    {
        private Project project = new Project();
        private ProjectService projectService;
        public int ClientId { get; set; }

        public ProjectAddViewModel()
        {
            projectService = new ProjectService();
        }

        public ICommand SaveProject
        {
            get { return new DelegateCommand<object>(FuncToCall); }
        }

        private void FuncToCall(object context)
        {
            project.ClientId = ClientId;
            project.Name = TxtName;
            //project.Category = TxtCategory;
            project.Description = TxtDescription;

            projectService.InsertProject(project);
        }
        public string TxtName
        {
            get { return project.Name; }
            set { project.Name = value; }
        }

        public string TxtDescription
        {
            get { return project.Description; }
            set { project.Description = value; }
        }
        //public string TxtCategory
        //{
        //    get { return project.Category; }
        //    set { project.Category = value; }
        //}
    }
}
