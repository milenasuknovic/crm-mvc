﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System.Windows;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    public class UpdateProjectViewModel
    {
        private Project _project = new Project();
        private ProjectService projectService;
        public MainViewModel mainView;

        public UpdateProjectViewModel(MainViewModel mainView, Project project)
        {
            projectService = new ProjectService();
            _project = project;
            this.mainView = mainView;
        }

        public ICommand UpdateProject
        {
            get { return new DelegateCommand<object>(FuncToCall); }
        }

        private void FuncToCall(object context)
        {
            TxtName = _project.Name;
            //TxtCategory = _project.Category;
            TxtDescription = _project.Description;

            mainView.Projects.Remove(_project);
            projectService.UpdateProject(_project);
            mainView.Projects.Add(_project);

            MessageBox.Show("Project updated!");
        }
        public string TxtName
        {
            get { return _project.Name; }
            set { _project.Name = value; }
        }

        public string TxtDescription
        {
            get { return _project.Description; }
            set { _project.Description = value; }
        }

        //public string TxtCategory
        //{
        //    get { return _project.Category; }
        //    set { _project.Category = CRM.Domain.Enums.Category.Internal; }
        //}
    }
}
