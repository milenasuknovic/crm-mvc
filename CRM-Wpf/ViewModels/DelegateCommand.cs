﻿using System;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    internal class DelegateCommand<T> : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;
        public event EventHandler CanExecuteChanged;
        private Action<object> funcToCall;
        private ICommand searchClient;

        public DelegateCommand(Action<object> funcToCall, Action<object> execute) : this(execute, null)
        {
            this.funcToCall = funcToCall;
        }

        public DelegateCommand(Action<object> execute)
                  : this(execute, null)
        {
        }

        public DelegateCommand(Action<object> execute,
                       Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public DelegateCommand(ICommand searchClient)
        {
            this.searchClient = searchClient;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute == null)
            {
                return true;
            }

            return _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
