﻿using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CRM_Wpf.ViewModels
{
    public class ClientAddViewModel : INotifyPropertyChanged
    {
        private Client _client = new Client();
        private ClientService _clientService;

        public event PropertyChangedEventHandler PropertyChanged;

        public ClientAddViewModel()
        {
            _clientService = new ClientService();
        }

        public ICommand SaveClient
        {
            get { return new DelegateCommand<object>(FuncToCall); }
        }

        private void FuncToCall(object context)
        {
            _client.Address = TxtAddress;
            _client.City = TxtCity;
            _client.Name = TxtName;
            _client.Notes = TxtNotes;
            _client.PhoneNumber = TxtPhoneNumber;
            _client.State = TxtState;
            _client.Website = TxtWebsite;
            _client.ZipCode = TxtZipCode;
            _clientService.InsertClient(_client);

            MessageBox.Show("Client added!");
        }

        public string TxtName
        {
            get { return _client.Name; }
            set { _client.Name = value; }
        }

        public string TxtAddress
        {
            get { return _client.Address; }
            set { _client.Address = value; }
        }

        public string TxtCity
        {
            get { return _client.City; }
            set { _client.City = value; }
        }

        public string TxtZipCode
        {
            get { return _client.ZipCode; }
            set { _client.ZipCode = value; }
        }

        public string TxtState
        {
            get { return _client.State; }
            set { _client.State = value; }
        }

        public string TxtPhoneNumber
        {
            get { return _client.PhoneNumber; }
            set { _client.PhoneNumber = value; }
        }

        public string TxtWebsite
        {
            get { return _client.Website; }
            set { _client.Website = value; }
        }

        public string TxtNotes
        {
            get { return _client.Notes; }
            set { _client.Notes = value; }
        }
    }
}
