﻿using CRM_Wpf.ViewModels;
using System;
using System.Windows;


namespace CRM_Wpf
{
    /// <summary>
    /// Interaction logic for AddContact.xaml
    /// </summary>
    public partial class AddContact : Window
    {
        public AddContact(MainViewModel main, int clientId)
        {
            InitializeComponent();
            DataContext = new AddContactViewModel(main, clientId);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
