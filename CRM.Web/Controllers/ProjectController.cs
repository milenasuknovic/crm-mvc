﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM.Data.EF;
using CRM.Web.Models;
using CRM.Services.BusinessServices;
using AutoMapper;
using CRM.Domain.Entities;
using System.Web.Routing;

namespace CRM.Web.Controllers
{
    [Route("Project")]
    public class ProjectController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IProjectService _projectService;
        private readonly IMapper _mapper;

        public ProjectController(IClientService clientService, IProjectService projectService, IMapper mapper)
        {
            this._clientService = clientService;
            this._projectService = projectService;
            this._mapper = mapper;
        }

        [HttpGet]
        public ActionResult Index(int? clientId, string SearchName)
        {
            if (clientId.HasValue)
                return View(new ClientForContactModel { Id = clientId.Value });

            return Json(new { Success = "True" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetJsonProjects(int clientId, string SearchName)
        {
            List<Project> projects = null;

            if (!string.IsNullOrEmpty(SearchName))
            {
                projects = _projectService.GetAllProjects(SearchName, clientId).ToList();
            }
            else
            {
                projects = _projectService.GetAllProjects(clientId).ToList();

            }
            var projctModel = _mapper.Map<List<ProjectModel>>(projects);

            return Json(projctModel.AsEnumerable(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddProject(int clientId)
        {
            var project = new Project();
            project.ClientId = clientId;
            var result = _mapper.Map<ProjectModel>(project);

            return View(result);
        }

        [HttpPost]
        public ActionResult AddProject(ProjectModel projectModel)
        {
            var project = _mapper.Map<Project>(projectModel);
            _projectService.InsertProject(project);

            return Json(new { Success = true, RedirectUrl = Url.Action("Index", "Project", new { clientId = project.ClientId }) });
        }

        [HttpGet]
        [Route("/EditProject/{id}")]
        public ActionResult EditProject(int id)
        {
            var project = _projectService.GetProjectById(id);
            if (project == null)
            {
                throw new HttpException(404, "Project not found!");
            }

            var result = _mapper.Map<ProjectModel>(project);

            return View(result);
        }

        [HttpPut]
        [Route("/EditProject")]
        public ActionResult EditProject(ProjectModel projectModel)
        {
            var project = _mapper.Map<Project>(projectModel);
            _projectService.UpdateProject(project);

            return Json(new { Success = true, RedirectUrl = Url.Action("Index", "Project", new { clientId = project.ClientId }) });
        }

        [HttpDelete]
        [Route("/DeleteProject/{id}")]
        public ActionResult DeleteProject(int id)
        {
            var project = _projectService.GetProjectById(id);
            if (project == null)
            {
                throw new HttpException(404, "Project not found!");
            }

            _projectService.DeleteProject(project);

            return Json(new { Success = "True" }, JsonRequestBehavior.AllowGet);
        }
    }
}
