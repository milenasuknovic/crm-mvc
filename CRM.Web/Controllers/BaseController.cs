﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Web.Controllers
{
    public class BaseController : Controller
    {
        protected JsonResult RedirectFromAjax(String actionName, String controllerName, object routeValues)
        {
            return Json(new { Success = true, RedirectUrl = Url.Action(actionName, controllerName, routeValues) });
        }

        protected JsonResult FalseAjaxResponse(String error)
        {
            return Json(new { Success = false, Error = error });
        }
    }
}