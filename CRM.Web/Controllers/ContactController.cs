﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using CRM.Web.Models;
using CRM.Services.BusinessServices;
using AutoMapper;
using CRM.Domain.Entities;
using System.Web.Routing;
using System.Web;

namespace CRM.Web.Controllers
{
    [Route("Contact")]
    public class ContactController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IContactService _contactService;
        private readonly IMapper _mapper;

        public ContactController(IClientService clientService, IContactService contactService, IMapper mapper)
        {
            this._clientService = clientService;
            this._contactService = contactService;
            this._mapper = mapper;
        }

        [HttpGet]
        public ActionResult Index(int? clientId, string SearchName)
        {
            if (clientId.HasValue)
                return View(new ClientForContactModel { Id = clientId.Value });

            return Json(new { Success = "True" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetJsonContacts(int clientId, string SearchName)
        {
            List<Contact> contacts = null;

            if (!string.IsNullOrEmpty(SearchName))
            {
                contacts = _contactService.GetAllContact(SearchName, clientId).ToList();
            }
            else
            {
                contacts = _contactService.GetAllContact(clientId).ToList();
            }
            var contactModel = _mapper.Map<List<ContactModel>>(contacts);

            return Json(contactModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddContact(int clientId)
        {
            var contact = new Contact();
            contact.ClientId = clientId;

            var result = _mapper.Map<ContactModel>(contact);

            return View(result);
        }

        [HttpPost]
        public ActionResult AddContact(ContactModel contactModel)
        {
            Contact contact = _mapper.Map<Contact>(contactModel);
            _contactService.InsertContact(contact);

            return Json(new { Success = true, RedirectUrl = Url.Action("Index", "Contact", new { clientId = contact.ClientId }) });
        }

        [HttpGet]
        [Route("/EditContact/{id}")]
        public ActionResult EditContact(int id)
        {
            var contact = _contactService.GetContactById(id);
            if (contact == null)
            {
                throw new HttpException(404, "Contact not found!");
            }

            var result = _mapper.Map<ContactModel>(contact);

            return View(result);
        }

        [HttpPut]
        [Route("/EditContact")]
        public ActionResult EditContact(ContactModel contactModel)
        {
            Contact contact = _mapper.Map<Contact>(contactModel);
            _contactService.UpdateContact(contact);

            return Json(new { Success = true, RedirectUrl = Url.Action("Index", "Contact", new { clientId = contact.ClientId }) });
        }

        [HttpDelete]
        [Route("/DeleteContact/{id}")]
        public ActionResult DeleteContact(int id)
        {
            var contact = _contactService.GetContactById(id);
            if (contact == null)
            {
                throw new HttpException(404, "Contact not found!");
            }

            _contactService.DeleteContact(contact);

            return Json(new { Success = "True" }, JsonRequestBehavior.AllowGet);
        }
    }
}
