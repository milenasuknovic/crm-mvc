﻿using AutoMapper;
using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using CRM.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Web.Controllers
{
    [Route("Client")]
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;

        public ClientController(IClientService clientService, IMapper mapper)
        {
            this._clientService = clientService;
            this._mapper = mapper;
        }

        [HttpGet]
        public ActionResult Index(string SearchName)
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetJsonClients(string SearchName)
        {
            List<Client> clients = null;

            if (!string.IsNullOrEmpty(SearchName))
            {
                clients = _clientService.GetAllClients(SearchName).ToList();
            }
            else
            {
                clients = _clientService.GetAllClients().ToList();
            }

            var clientsModel = _mapper.Map<List<ClientModel>>(clients);

            return Json(clientsModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("/AddClient")]
        public ActionResult AddClient()
        {
            return View(new ClientModel());
        }

        [HttpPost]
        [Route("/AddClient")]
        public ActionResult AddClient(ClientModel clientModel)
        {
            var client = _mapper.Map<Client>(clientModel);
            _clientService.InsertClient(client);

            return Json(new { Success = true, RedirectUrl = Url.Action("Index", "Client", new { }) });
        }

        [HttpGet]
        [Route("/EditClient/{id}")]
        public ActionResult EditClient(int id)
        {
            var client = _clientService.GetClientById(id);
            if (client == null)
            {
                throw new HttpException(404, "Client not found!");
            }

            var result = _mapper.Map<ClientModel>(client);

            return View(result);
        }

        [HttpPut]
        [Route("/EditClient")]
        public ActionResult EditClient(ClientModel clientModel)
        {
            var client = _mapper.Map<Client>(clientModel);
            _clientService.UpdateClient(client);

            return Json(new { Success = true, RedirectUrl = Url.Action("Index", "Client", new { }) });
        }


        [HttpDelete]
        [Route("/DeleteClient/{id}")]
        public ActionResult DeleteClient(int id)
        {
            var client = _clientService.GetClientById(id);
            if (client == null)
            {
                throw new HttpException(404, "Client not found!");
            }
            _clientService.DeleteClient(client);

            return Json(new { Success = "True" }, JsonRequestBehavior.AllowGet);
        }
    }
}