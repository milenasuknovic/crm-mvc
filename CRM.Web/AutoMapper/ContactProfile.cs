﻿using AutoMapper;
using CRM.Domain.Entities;
using CRM.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Web.AutoMapper
{
    public class ContactProfile : Profile
    {
        public ContactProfile()
        {
            CreateMap<Contact, ContactModel>();
            CreateMap<ContactModel, Contact>();
        }
    }
}