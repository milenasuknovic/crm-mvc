﻿using AutoMapper;
using CRM.Domain.Entities;
using CRM.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Web.AutoMapper
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectModel>();
            CreateMap<ProjectModel, Project>();
        }
    }
}