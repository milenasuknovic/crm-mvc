﻿using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using AutoMapper.XpressionMapper;
using CRM.Data.EF;
using CRM.Domain.Entities;
using CRM.Services.BusinessServices;
using CRM.Web.AutoMapper;
using CRM.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CRM.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            builder.RegisterModule(new AutoMapperModule());

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.

            builder.RegisterType(typeof(ClientService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ClientAccess)).AsImplementedInterfaces();

            builder.RegisterType(typeof(ContactService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ContactAccess)).AsImplementedInterfaces();

            builder.RegisterType(typeof(ProjectService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ProjectAccess)).AsImplementedInterfaces();

            builder.RegisterFilterProvider();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            //Mappings.RegisterMappings();

            //var config = new MapperConfiguration(cfg =>
            //{
            //    cfg.CreateMap<Client, ClientModel>();
            //    //cfg.AddProfile<ClientProfile>();
            //});
        }
    }
}
