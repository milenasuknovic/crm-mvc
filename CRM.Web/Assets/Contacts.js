﻿var moduleContacts = (function ($) {

    var clientId = $('input#clientId').val();

    function init() {
        loadContacts();
        deleteContact();
        addContact();
        editContact();
    }

    function loadContacts() {

        var URL = null;

        var searchName = ((document.getElementById('name') || {}).value);

        if (searchName == null) {
            URL = "/Contact/GetJsonContacts?clientId=" + $('input#clientId').val();
        } else {
            URL = "/Contact/GetJsonContacts?clientId=" + clientId + " &SearchName=" + searchName;
        }

        $.getJSON(URL, function (data) {
            var result = "";
            $.each(data, function (i, item) {
                result += '<tr>';
                result += '<td>' + item.FirstName + '</td>';
                result += '<td>' + item.LastName + '</td>';
                result += '<td>' + item.Role + '</td>';
                result += '<td>' + item.Email + '</td>';
                result += '<td>' + item.Phone + '</td>';
                result += '<td>' + item.City + '</td>';
                result += '<td>' + item.ZipCode + '</td>';
                result += '<td>' + item.State + '</td>';
                result += '<td>' +
                    '<a href="/Contact/EditContact?Id=' + item.Id + '" class="btn btn-info">Edit</a>' +
                    '<button class="delete btn btn-info"  data-id=' + item.Id + '>Delete</button>' + '</td>';
            });
            $('#Contacts tbody').html(result);
        });
        $("#search-click").click(loadContacts);
    }


    function addContact() {
        $(document).on('click', ".add", function (e) {

            e.preventDefault();

            var firstName = $("input[name=FirstName]").val();
            var lastName = $("input[name=LastName]").val();
            var role = $("input[name=Role]").val();
            var email = $("input[name=Email]").val();
            var phone = $("input[name=Phone]").val();
            var address = $("input[name=Address]").val();
            var city = $("input[name=City]").val();
            var zipCode = $("input[name=ZipCode]").val();
            var state = $("input[name=State]").val();
            var clientId = document.getElementById("Id").value;

            if (confirm("Are you sure to want to add this contact?")) {
                $.ajax({
                    url: '/Contact/AddContact',
                    data: { firstName: firstName, lastName: lastName, role: role, email: email, phone: phone, address: address, city: city, zipCode: zipCode, state: state, clientId: clientId },
                    type: 'POST',
                    dataType: "json",
                    success: function () {
                        window.location.href = "http://localhost:51839";
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            }
        });
    }

    function editContact() {
        $(document).on('click', ".edit", function () {

            var id = document.getElementById("Id").value;
            var firstName = $("input[name=FirstName]").val();
            var lastName = $("input[name=LastName]").val();
            var role = $("input[name=Role]").val();
            var email = $("input[name=Email]").val();
            var phone = $("input[name=Phone]").val();
            var address = $("input[name=Address]").val();
            var city = $("input[name=City]").val();
            var zipCode = $("input[name=ZipCode]").val();
            var state = $("input[name=State]").val();
            var clientId = document.getElementById("ClientId").value;

            if (confirm("Are you sure to want to edit this contact?")) {
                $.ajax({
                    url: '/Contact/EditContact',
                    data: { id: id, firstName: firstName, lastName: lastName, role: role, email: email, phone: phone, address: address, city: city, zipCode: zipCode, state: state, clientId: clientId },
                    type: 'PUT',
                    dataType: "json",
                    success: function () {
                        window.location.href = "http://localhost:51839";
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            }
        });
    }

    function deleteContact() {
        $(document).on('click', ".delete", function (e) {

            e.preventDefault();

            if (confirm("Are you sure to want to delete this contact?")) {
                var Id = $(this).attr('data-id');
                $.ajax({
                    url: '/Contact/DeleteContact/' + Id,
                    type: 'DELETE',
                    dataType: "json",
                    success: function () {
                        loadContacts();
                    }
                });
            }
        });
    }

    return {
        init: init
    };

})(jQuery);
jQuery(document).ready(moduleContacts.init);