﻿var util = (function ($) {

    function ajaxRedirect(response) {

        if (response.Success) {
            window.location = response.RedirectUrl;
        }
    }

    return {

        ajaxRedirect: ajaxRedirect
    };

})(jQuery);
