﻿var moduleProjects = (function ($) {

    var clientId = $('input#clientId').val();

    function init() {
        loadProjects();
        deleteProject();
        addProject();
        editProject();
    }

    function loadProjects() {

        var URL = null;
        
        var searchName = ((document.getElementById('name') || {}).value);

        if (searchName == null) {
            URL = "/Project/GetJsonProjects?clientId=" + $('input#clientId').val()
        } else {
            URL = "/Project/GetJsonProjects?clientId=" + clientId + " &SearchName=" + searchName;
        }

        $.getJSON(URL, function (data) {
            var result = "";
            $.each(data, function (i, item) {
                result += '<tr>';
                result += '<td>' + item.Name + '</td>';
                result += '<td>' + item.Description + '</td>';
                result += '<td>' + item.Category + '</td>';
                result += '<td>' +
                    '<a href="/Project/EditProject?Id=' + item.Id + '" class="btn btn-info">Edit</a>' +
                    '<button class="delete btn btn-info" data-id=' + item.Id + '>Delete</button>' + '</td>';
            });
            $('#Projects tbody').html(result);
        });
        $("#search-click").click(loadProjects);
    }

    function deleteProject() {

        $(document).on('click', ".delete", function (e) {

            e.preventDefault();

            if (confirm("Are you sure to want to delete this project?")) {
                var Id = $(this).attr('data-id');
                $.ajax({
                    url: '/Project/DeleteProject/' + Id,
                    type: 'DELETE',
                    dataType: "json",
                    success: function () {
                        loadProjects();
                    }
                });
            }
        });
    }

    function addProject() {
        $(document).on('click', ".add", function () {

            var name = $("input[name=Name]").val();
            var description = $("input[name=Description]").val();
            var category = $("input[name=Category]").val();
            var clientId = document.getElementById('ClientId').value;

            if (confirm("Are you sure to want to add this project?")) {
                $.ajax({
                    url: '/Project/AddProject',
                    data: { name: name, description: description, category: category, clientId: clientId },
                    type: 'POST',
                    dataType: "json",
                    success: function () {
                        window.location.href = "http://localhost:51839";
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            }
        });
    }

    function editProject() {
        $(document).on('click', ".edit", function (e) {

            e.preventDefault();

            var id = document.getElementById('Id').value;
            var name = $("input[name=Name]").val();
            var description = $("input[name=Description]").val();
            var category = $("input[name=Category]").val();
            var clientId = document.getElementById('ClientId').value;

            if (confirm("Are you sure to want to edit this project?")) {
                $.ajax({
                    url: '/Project/EditProject',
                    data: { id: id, name: name, description: description, category: category, clientId: clientId },
                    type: 'PUT',
                    dataType: "json",
                    success: function () {
                        window.location.href = "http://localhost:51839";
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            }
        });
    }

    return {
        init: init
    };

})(jQuery);
jQuery(document).ready(moduleProjects.init);