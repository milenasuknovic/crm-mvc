﻿var module = (function ($) {

    function init() {
        add();
        loadClients();
        deleteClient();
        addClient();
        editClient();
    }

    function add() {
        $(document).on('click', ".add", function () {
            $("#ajaxForm").submit(function (event) {
                var dataString;
                event.preventDefault();
                event.stopImmediatePropagation();
                var action = $("#ajaxForm").attr("action");
                alert(action);
                dataString = new FormData($("#ajaxForm").get(0));
                $.ajax({
                    type: "POST",
                    url: action,
                    data: dataString,
                    dataType: "json",
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("fail");
                    }
                });
            });
        });
    }

    function loadClients() {

        var URL = null;

        var searchName = ((document.getElementById('name') || {}).value);

        if (searchName == null) {
            URL = "/Client/GetJsonClients/";
        }
        else {
            URL = "/Client/GetJsonClients?SearchName=" + searchName;
        }

        $.getJSON(URL, function (data) {
            var result = "";
            $.each(data, function (i, item) {
                result += '<tr>';
                result += '<td>' + item.Id + '</td>';
                result += '<td>' + item.Name + '</td>';
                result += '<td>' + item.Address + '</td>';
                result += '<td>' + item.City + '</td>';
                result += '<td>' + item.ZipCode + '</td>';
                result += '<td>' + item.State + '</td>';
                result += '<td>' + item.PhoneNumber + '</td>';
                result += '<td>' + item.Website + '</td>';
                result += '<td>' + item.Notes + '</td>';
                result += '<td>' +
                    '<a href="/Client/EditClient?Id=' + item.Id + '" class="btn btn-info">Edit</a>' +
                    '<button class="delete btn btn-info" data-id=' + item.Id + '>Delete</button>' +
                    '</td>';
                result += '<td>' +
                    ' <a href="/Contact/Index?clientId=' + item.Id + '" data-id=' + item.Id + ' class="btn btn-info" >Contacts</a>' +
                    '</td>';
                result += '<td>' +
                    '<a href="/Project/Index?clientId=' + item.Id + '" data-id=' + item.Id + ' class="btn btn-info" >Projects</a>' +
                    '</td>';
            });

            $('#Clients tbody').html(result);
        });
        $("#search-click").click(loadClients);
    }

    function deleteClient() {
        $(document).on('click', ".delete", function (e) {

            e.preventDefault();

            if (confirm("Are you sure to want to delete this client?")) {
                var Id = $(this).attr('data-id');
                $.ajax({
                    url: '/Client/DeleteClient/' + Id,
                    type: 'DELETE',
                    dataType: "json",
                    success: function () {
                        loadClients();
                    }
                })
            };
        });
    }

    function addClient() {
        $(document).on('click', ".add", function (e) {

            e.preventDefault();


            if (confirm("Are you sure to want to add this client?")) {

                $.ajax({
                    url: '/Client/AddClient',
                    data: { name: name, address: address, city: city, zipCode: zipCode, state: state, phoneNumber: phoneNumber, website: website, notes: notes },
                    type: 'POST',
                    dataType: "json",
                    success: function () {
                        window.location.href = "http://localhost:51839";
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                })
            }
        });
    }

    function editClient() {
        $(document).on('click', ".edit", function (e) {

            e.preventDefault();

            var id = document.getElementById("Id").value;
            var name = $("input[name=Name]").val();
            var address = $("input[name=Address]").val();
            var city = $("input[name=City]").val();
            var zipCode = $("input[name=ZipCode]").val();
            var state = $("input[name=State]").val();
            var phoneNumber = $("input[name=PhoneNumber]").val();
            var website = $("input[name=Website]").val();
            var notes = $("input[name=Notes]").val();

            if (confirm("Are you sure to want to edit this client?")) {
                $.ajax({
                    url: '/Client/EditClient',
                    data: { id: id, name: name, address: address, city: city, zipCode: zipCode, state: state, phoneNumber: phoneNumber, website: website, notes: notes },
                    type: 'PUT',
                    dataType: "json",
                    success: function () {
                        window.location.href = "http://localhost:51839";
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            }
        });
    }



    return {
        init: init
    };

})(jQuery);
jQuery(document).ready(module.init);