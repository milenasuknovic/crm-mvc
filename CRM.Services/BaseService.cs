﻿using CRM.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services
{
    public class BaseService
    {
        protected CRMContext _dbContext;

        public BaseService()
        {
            _dbContext = new CRMContext();
        }
    }
}
