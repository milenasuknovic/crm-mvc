﻿using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.BusinessServices
{
    public interface IClientService
    {
        IEnumerable<Client> GetAllClients();
        IEnumerable<Client> GetAllClients(string search);
        bool InsertClient(Client client);
        bool UpdateClient(Client client);
        bool DeleteClient(Client client);
        Client GetClientById(int id);
    }
}
