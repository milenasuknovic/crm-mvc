﻿using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.BusinessServices
{
    public interface IContactService
    {
        IEnumerable<Contact> GetAllContact(string search, int clientId);
        IEnumerable<Contact> GetAllContact(int clientId);
        bool InsertContact(Contact contact);
        bool UpdateContact(Contact contact);
        bool DeleteContact(Contact contact);
        Contact GetContactById(int id);
    }

}
