﻿using CRM.Data.EF;
using CRM.Data.Interfaces;
using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CRM.Services.BusinessServices
{
    public class ContactService : BaseService, IContactService
    {
        private IContactAccess _memberAccess;

        public ContactService() : base()
        {
            this._memberAccess = new ContactAccess(_dbContext);
        }

        public bool InsertContact(Contact contact)
        {
            return this._memberAccess.Add(contact);
        }

        public bool UpdateContact(Contact contact)
        {
            return this._memberAccess.Update(contact);
        }

        public bool DeleteContact(Contact contact)
        {
            return this._memberAccess.Delete(contact);
        }

        public IEnumerable<Contact> GetAllContact(string search, int clientId)
        {
            return this._memberAccess.GetAllContact(clientId).Where(x => x.FirstName.Contains(search));
        }

        public Contact GetContactById(int id)
        {
            return this._memberAccess.GetById(id);
        }

        public IEnumerable<Contact> GetAllContact(int clientId)
        {
            return this._memberAccess.GetAllContact(clientId);
        }
    }
}
