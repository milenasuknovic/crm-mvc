﻿using CRM.Data.EF;
using CRM.Data.Interfaces;
using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.BusinessServices
{
    public class ProjectService : BaseService, IProjectService
    {
        private IProjectAccess _memberAccess;

        public ProjectService()
        {
            this._memberAccess = new ProjectAccess(_dbContext);
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return this._memberAccess.GetAll();
        }

        public bool InsertProject(Project project)
        {
            project.Category = Domain.Enums.Category.Internal;
            return this._memberAccess.Add(project);
        }

        public bool UpdateProject(Project project)
        {
            project.Category = Domain.Enums.Category.Internal;
            return this._memberAccess.Update(project);
        }

        public bool DeleteProject(Project project)
        {
            return this._memberAccess.Delete(project);
        }

        public IEnumerable<Project> GetAllProject(int id)
        {
            return this._memberAccess.GetAllProject(id);
        }

        public IEnumerable<Project> GetAllProjects(int clientId)
        {
            return this._memberAccess.GetAllProject(clientId);
        }

        public Project GetProjectById(int id)
        {
            return this._memberAccess.GetById(id);
        }

        public IEnumerable<Project> GetAllProjects(string search, int clientId)
        {
            return this._memberAccess.GetAllProject(clientId).Where(x => x.Name.Contains(search));
        }

    }
}
