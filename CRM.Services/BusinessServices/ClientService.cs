﻿using CRM.Data.EF;
using CRM.Data.Interfaces;
using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.BusinessServices
{
    public class ClientService : BaseService, IClientService
    {
        private IClientAccess _memberAccess;

        public ClientService() : base()
        {
            this._memberAccess = new ClientAccess(_dbContext);
        }

        public IEnumerable<Client> GetAllClients()
        {
            return this._memberAccess.GetAll();
        }

        public bool InsertClient(Client client)
        {
            client.IsDeleted = false;
            return this._memberAccess.Add(client);
        }

        public bool UpdateClient(Client client)
        {
            return this._memberAccess.Update(client);
        }

        public bool DeleteClient(Client client)
        {
            //client.IsDeleted = true;
            //_memberAccess.Update(client);

            return this._memberAccess.Delete(client);
        }

        public Client GetClientById(int id)
        {
            return this._memberAccess.GetById(id);
        }

        public IEnumerable<Client> GetAllClients(string search)
        {
            return _memberAccess.GetAll().Where(x => x.Name.Contains(search));
        }
    }
}
