﻿using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.BusinessServices
{
    public interface IProjectService
    {
        IEnumerable<Project> GetAllProjects(string search, int clientId);
        IEnumerable<Project> GetAllProjects(int clientId);
        bool InsertProject(Project project);
        bool UpdateProject(Project project);
        bool DeleteProject(Project project);
        Project GetProjectById(int id);
    }
}
