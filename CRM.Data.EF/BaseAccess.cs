﻿using CRM.Data.Interfaces;
using CRM.Domain;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CRM.Data.EF
{
    public abstract class BaseAccess<TEntity> : IAccess<TEntity> where TEntity : BaseEntity
    {
        protected readonly DbContext _dbContext;

        public BaseAccess(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>();
        }

        public bool Add(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            _dbContext.SaveChanges();
            return true;
        }

        public bool Update(TEntity entity)
        {
            _dbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            _dbContext.SaveChanges();
            return true;
        }

        public bool Delete(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            _dbContext.SaveChanges();
            return true;
        }

        public TEntity GetById(int id)
        {
            return _dbContext.Set<TEntity>().Find(id);
        }
    }
}
