﻿using CRM.Data.Interfaces;
using CRM.Domain.Entities;

namespace CRM.Data.EF
{
    public class ClientAccess : BaseAccess<Client>, IClientAccess
    {
        public ClientAccess(CRMContext dbContext) : base(dbContext)
        {
        }
    }
}
