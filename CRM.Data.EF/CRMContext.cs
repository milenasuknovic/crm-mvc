﻿using CRM.Domain.Entities;
using System.Data.Entity;


namespace CRM.Data.EF
{
    public class CRMContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Project> Projects { get; set; }

        public CRMContext()
            : base("crm")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .HasRequired(x => x.Client)
                .WithMany(x => x.Contacts)
                .HasForeignKey(x => x.ClientId);

            modelBuilder.Entity<Project>()
                .HasRequired(x => x.Client)
                .WithMany(x => x.Projects)
                .HasForeignKey(x => x.ClientId);
        }
    }
}
