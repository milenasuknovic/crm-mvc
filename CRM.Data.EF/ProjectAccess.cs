﻿using CRM.Data.Interfaces;
using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.EF
{
    public class ProjectAccess : BaseAccess<Project>, IProjectAccess
    {
        public ProjectAccess(CRMContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Project> GetAllProject(int id)
        {
            return _dbContext.Set<Project>().Where(e => e.ClientId == id);
        }
    }
}
