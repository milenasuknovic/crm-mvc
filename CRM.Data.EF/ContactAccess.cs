﻿using CRM.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using CRM.Domain.Entities;


namespace CRM.Data.EF
{
    public class ContactAccess : BaseAccess<Contact>, IContactAccess
    {
        public ContactAccess(CRMContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Contact> GetAllContact(int id)
        {
            return _dbContext.Set<Contact>().Where(e => e.ClientId == id);
        }
    }

}