namespace CRM.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCategoryEnum : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Projects", "Category", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Projects", "Category", c => c.String());
        }
    }
}
