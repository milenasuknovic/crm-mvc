namespace CRM.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsDeletedProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "IsDeleted");
            DropColumn("dbo.Contacts", "IsDeleted");
            DropColumn("dbo.Clients", "IsDeleted");
        }
    }
}
