﻿using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CRM.Data.Interfaces
{
    public interface IProjectAccess : IAccess<Project>
    {
        IEnumerable<Project> GetAllProject(int id);
    }
}
