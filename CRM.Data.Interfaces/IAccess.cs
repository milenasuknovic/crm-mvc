﻿using CRM.Domain;
using System.Collections.Generic;
using System.Linq;

namespace CRM.Data.Interfaces
{
    public interface IAccess<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> GetAll();
        bool Add(TEntity t);
        bool Update(TEntity t);
        bool Delete(TEntity t);
        TEntity GetById(int id);
    }
}
