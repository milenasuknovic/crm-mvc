﻿using CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CRM.Data.Interfaces
{
    public interface IContactAccess : IAccess<Contact>
    {
        IEnumerable<Contact> GetAllContact(int id);
    }
}
