﻿using CRM.Domain.Entities;


namespace CRM.Data.Interfaces
{
    public interface IClientAccess : IAccess<Client>
    {
    }
}
